# Copyright (C) The Arvados Authors. All rights reserved.
#
# SPDX-License-Identifier: Apache-2.0

# Lock in a specific image.
FROM phusion/passenger-ruby25:1.0.9

# Set correct environment variables (from https://github.com/phusion/passenger-docker).
# TODO(jacoberts): This breaks configuration file reading in a mysterious way (confirmed):
# 2020-01-21T20:32:02.989947343Z Refusing to start in production mode with missing configuration.
# 2020-01-21T20:32:02.98994958Z 
# 2020-01-21T20:32:02.989951667Z The following configuration settings must be specified in
# 2020-01-21T20:32:02.989953899Z config/application.yml:
# 2020-01-21T20:32:02.989956143Z * arvados_login_base
# 2020-01-21T20:32:02.989958222Z * arvados_v1_base
# 2020-01-21T20:32:02.989960286Z * arvados_insecure_https
# 2020-01-21T20:32:02.98996253Z * secret_token
# 
# ENV HOME /root

# Phusion configuration: we're using a "customizable" variant, so get ruby
# set up:
#RUN ls /pd_build
#RUN /pd_build/ruby-2.5.*.sh

RUN apt-get update && apt-get install -y gnupg2

# put nginx in daemon mode again; the arvados-workbench package restarts
# nginx, which would lead to a hang otherwise...
RUN sed -i 's/daemon off;/#daemon off;/' /etc/nginx/nginx.conf

ADD 1078ECD7.asc /tmp/
RUN cat /tmp/1078ECD7.asc | apt-key add -

RUN echo "deb http://apt.arvados.org/ bionic main" > /etc/apt/sources.list.d/apt.arvados.org.list

RUN apt-get update && apt-get install -qqy tzdata

# preinstall latest arvados rails packages, so that we have (most of) the gems
# baked into this docker image. Then remove them so that this image is generic.
# The bundles will remain installed.
RUN apt-get install -qqy arvados-workbench arvados-sso-server arvados-api-server
RUN apt-get remove -qqy arvados-workbench arvados-sso-server arvados-api-server

# put nginx.conf back into the state it needs to be
RUN sed -i 's/#daemon off;/daemon off;/' /etc/nginx/nginx.conf

RUN apt-get install -qqy postgresql-client

# Remove the default site and enable nginx.
RUN rm /etc/nginx/sites-enabled/default
RUN rm /etc/service/nginx/down

COPY bootstrap.sh /usr/local/bin/bootstrap.sh
