#!/bin/bash
# Copyright (C) The Arvados Authors. All rights reserved.
#
# SPDX-License-Identifier: Apache-2.0

set -e
export HOME="/root"
export ARVADOS_API_TOKEN={{ .Values.superUserSecret }}
# TODO(jacoberts): Maybe route internally?
export ARVADOS_API_HOST={{ .Values.domain }}
export ARVADOS_API_HOST_INSECURE="true"
arv keep_service create --keep-service "$(cat <<EOF
{
 "service_host":"arvados-keep-store-0.arvados-keep-store",
 "service_port":25107,
 "service_ssl_flag":false,
 "service_type":"disk"
}
EOF
)"

arv keep_service create --keep-service "$(cat <<EOF
{
 "service_host":"arvados-keep-store-1.arvados-keep-store",
 "service_port":25107,
 "service_ssl_flag":false,
 "service_type":"disk"
}
EOF
)"

# TODO(jacoberts): Maybe route internally?
arv keep_service create --keep-service "$(cat <<EOF
{
 "service_host":"keep.{{ .Values.domain }}",
 "service_port":443,
 "service_ssl_flag":true,
 "service_type":"proxy"
}
EOF
)"
